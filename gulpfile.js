 
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('sass', function () {
	return gulp.src('./app/scss/*.scss')
	.pipe(sass({outputStyle: 'compressed'}))
	.on('error', function(err){
		browserSync.notify(err.message, 3000);
		this.emit('end');
	})
	.pipe(autoprefixer(autoprefixerOptions))
	.pipe(gulp.dest('./dist/css/'))
	.pipe(browserSync.stream());
});

gulp.task('html', function() {	
	gulp.src('./app/*.html')
	.pipe(gulp.dest('./dist/'))
	.pipe(browserSync.stream());
});

gulp.task('javascript', function() {	
	gulp.src('./app/js/**.js')
	.pipe(gulp.dest('./dist/js'))
	.pipe(browserSync.stream());
});
gulp.task('image', function() {	
	gulp.src('./app/img/*.**')
	.pipe(gulp.dest('./dist/img'))
	.pipe(browserSync.stream());
});

gulp.task('serve', ['sass', 'html', 'javascript', 'image'], function() {

	browserSync.init({
		server: "./dist"
	});

	gulp.watch("./app/scss/**/*.scss", ['sass']);
	gulp.watch("./app/*.html", ['html']);
	gulp.watch("./app/js/**.js", ['javascript']);
	gulp.watch("./app/img/*.**", ['image']);
});

gulp.task('default', ['serve']);