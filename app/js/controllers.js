var plataformas = angular.module('celularDireto', []);

plataformas.controller('celularDiretoCtrl', function($scope, $http) {
	var plataformas = function() {
		$http({
			method: 'GET',
			url: 'http://private-59658d-celulardireto2017.apiary-mock.com/plataformas'
		}).then(function (response){
			$scope.plataformas = response.data;
		});
	}
	var planosTablet = function() {
		$http({
			method: 'GET',
			url: 'http://private-59658d-celulardireto2017.apiary-mock.com/planos/TBT01'
		}).then(function (response){
			$scope.planosTablet = response.data;
		});
	}
	var planosDesktop = function() {
		$http({
			method: 'GET',
			url: 'http://private-59658d-celulardireto2017.apiary-mock.com/planos/CPT02'
		}).then(function (response){
			$scope.planosDesktop = response.data;
		});
	}
	var planosWifi = function() {
		$http({
			method: 'GET',
			url: 'http://private-59658d-celulardireto2017.apiary-mock.com/planos/WF03'
		}).then(function (response){
			$scope.planosWifi = response.data;
		});
	}
	$scope.dados =[];
	$scope.adicionarDados = function(nome, email, data, cpf, telefone) {
		$scope.dados.push({nome: nome, email: email, data: data, cpf: cpf, telefone: telefone});
		console.log($scope.dados);
	}
	plataformas();
	planosTablet();
	planosDesktop();
	planosWifi();
});
