var sku_aparelho = '',
    sku_plano = '';

$(document).on("click", ".tab__handle", function() {
	event.preventDefault();
	var target = $(this).attr('id');
	$('.tab__wrap').removeClass('active');
	$('.tab__wrap.' + target).addClass('active');
	$("html, body").stop().animate({scrollTop:$('.tab__wrap.active').offset().top}, 500, 'swing');
	sku_aparelho = $(this).find('.card__reference').text();
});
$(document).on("click", ".tab__wrap.active .card__container ", function() {
	event.preventDefault();
	$('.form__container').addClass('active');
	$("html, body").stop().animate({scrollTop:$('.form__container.active').offset().top}, 500, 'swing');
	sku_plano = $(this).find('.card__reference').text();
});

$(document).on("click", ".form__container .form__button ", function() {
	console.log("o SKU do aparelho selecionado é: " + sku_aparelho);
	console.log("o SKU do plano selecionado é: " + sku_plano);
});